package org.carpenter.bill;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

/*
 Expects to be called with two arguments, an input file and an output directory.
 Not especially robust because it's just a one-off. Expects an input file that
 looks like this:

id=aliceblue
     #f0f8ff
     240,248,255
id=antiquewhite
     #faebd7
     250,235,215
id=aqua
     #00ffff
     0,255,255

I produced the input file by copying that section out of the CSS
color spec HTML and flogging it with emacs. I then wrote this Java
code because I wanted to convert the 0-255 RGB values to floats.
There must be an emacs way to do that, but Java was faster for me.

Writes out a file for each color and another file that contains all of
the colors, all of that in esphome color spec format.
*/


public class RgbStuff {

  private static String outdirName;
  private static Writer allOut;

  public static void main(String[] args) throws IOException {
    if (args.length < 2) {
      throw new IllegalArgumentException("... inputFile outputDir");
    }
    final String fileName = args[0];
    RgbStuff.outdirName = args[1];
    if (!RgbStuff.outdirName.endsWith("/")) RgbStuff.outdirName += "/";
    RgbStuff.allOut = new OutputStreamWriter(new FileOutputStream(RgbStuff.outdirName + "COLOR_CSS"));
    RgbStuff.writeFileHeader(RgbStuff.allOut);
    final LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(fileName)));
    String idLine;
    while ((idLine = lnr.readLine()) != null) {
      // id=foo
      if (!idLine.contains("id=")) {
        throw new IllegalStateException("expected id=, saw " + idLine);
      }
      idLine = idLine.trim();
      String hexLine = lnr.readLine();
      if (hexLine == null  ||  !hexLine.contains("#")) {
        throw new IllegalStateException("expected hex, saw " + hexLine);
      }
      hexLine = hexLine.trim();
      String rgbLine = lnr.readLine();
      if (rgbLine == null  ||  !rgbLine.contains(",")) {
        throw new IllegalStateException("expected r,g,b, saw " + rgbLine);
      }
      rgbLine = rgbLine.trim();

      RgbStuff.emit(idLine, hexLine, rgbLine);
    }
    RgbStuff.allOut.close();
  }

  private static void emit(String idLine, String hexLine, String rgbLine) throws IOException {
    final String id = "COLOR_CSS_" + idLine.substring(3).toUpperCase();
    final String[] rgb = rgbLine.split(",");
    final double red   = Float.parseFloat(rgb[0]) / 255.0;
    final double green = Float.parseFloat(rgb[1]) / 255.0;
    final double blue  = Float.parseFloat(rgb[2]) / 255.0;

    final String outName = RgbStuff.outdirName + id;
    try (Writer out = new OutputStreamWriter(new FileOutputStream(outName))) {
      RgbStuff.writeFileHeader(out);
      RgbStuff.writeALine(out, "\n  # " + hexLine);
      RgbStuff.writeALine(out, "  # " + rgbLine);
      RgbStuff.writeALine(out, "  - id: " + id);
      RgbStuff.writeALine(out, String.format("    red:   %.4f\n    green: %.4f\n    blue:  %.4f\n    white: 0.0000", red, green, blue));
    }
  }

  private static void writeALine(Writer out, String line) throws IOException {
    out.write   (line + "\n");
    RgbStuff.allOut.write(line + "\n");
  }
  private static void writeFileHeader(Writer out) throws IOException {
    out.write("color:\n");
    out.write("  # name and values from https://www.w3.org/wiki/CSS/Properties/color/keywords\n");
  }
}
