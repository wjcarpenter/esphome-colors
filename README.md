# esphome-colors

Definitions for the CSS standard color names for use in ESPhome configurations.

ESPhome defines a `color:` component, where you can define RGB or RGBW color definitions for use with various other components. 
However, there are only two pre-defined colors, `COLOR_BLACK` and `COLOR_WHITE`. 
This repository provides definitions for all of the CSS standard color names. 
There are lots of lists of color names around, many of which overlap or conflict with each other. 
I chose to use the CSS color names because they are well-defined in a popular and familiar specification, and they contain a medium list of about 140 or so colors.

For every CSS color name `polkadot`, there is an ESPhome color definition with an ID of `COLOR_CSS_POLKADOT`. 
I used that naming scheme in the hope of avoiding naming conflicts with other things that might be used in an ESPhome configuration file.
It's still not guaranteed to avoid conflicts, but it should at least be easy to deal with.
Color definitions are in individual files with the same name as the color (with no filename extension).
There is also a file `COLOR_CSS` that contains all of the color definitions from the individual files.

RGB values are given as decimal fractions, and the `W` channel (white) has a value of 0.0 for all of them.

You can put these files wherever you want, but I put them in a `color` directory that is a peer to my ESPhome `config` directory.
The examples below reflect that.
If you want to do the same thing, you can clone this repository and symlink the `color` directory to the equivalent location in your environment.

There are a couple of different ways that you could use these color defintions.

- First, obviously, you could just copy the definitions of interest into the `color:` section of your ESPhome config file.
```
color:
  - id: COLOR_CSS_TOMATO
    red:   1.0000
    green: 0.3882
    blue:  0.2784
    white: 0.0000
  - id: COLOR_CSS_FIREBRICK
    red:   0.6980
    green: 0.1333
    blue:  0.1333
    white: 0.0000 
```

- Second, if you don't care about unused `Color` objects in the generated firmware, 
you could just include the entire `COLOR_CSS` file, via the YAML `<<: !include` mechanism:
```
<<: !include ../color/COLOR_CSS
```
or the ESPhome `packages:` mechanism:
```
packages:
  colors: !include ../color/COLOR_CSS
```
With either of those, the entire list of color names is immediately available.
That's handy if you are doing some experimenting before settling on the colors for your project.

- Third, you can use the ESPhome `packages:` mechanism to include exactly the colors you want to define:
```
packages:
   black:  !include ../color/COLOR_CSS_BLACK
   white:  !include ../color/COLOR_CSS_WHITE
   red:    !include ../color/COLOR_CSS_RED
   green:  !include ../color/COLOR_CSS_GREEN
   cyan:   !include ../color/COLOR_CSS_CYAN
   blue:   !include ../color/COLOR_CSS_BLUE
   yellow: !include ../color/COLOR_CSS_YELLOW
```
(The names `black`, `white`, `red`, etc, are arbitrary and just need to be unique to satisfy YAML rules.)

There are a few more files in this repository that you can just ignore.
`rawtable.txt` is a list of all of the colors with their hexadecimal codes and RGB 0-255 values.
I scraped the color table from the CSS specification and massaged it into that format.
The file RgbStuff.java is a one-off Java program that reads `rawtable.txt` and creates the other files in the repository.
